package generate;

import java.lang.reflect.Field;
import java.util.Date;


public class GenerateCopyDataMethodUtil {
	public static void main(String[] args) {
		generateCopyDataMethod(UserVo.class, UserDo.class);
	}
	
	public static void generateCopyDataMethod(Class<?> destClass,Class<?> sourceClass) {
		String destName = destClass.getSimpleName();
		String destNameL = toLowerCaseFirstOne(destName);
		String sourceName = sourceClass.getSimpleName();
		String sourceNameL = toLowerCaseFirstOne(sourceName);
		Field[] destFields = destClass.getDeclaredFields();
		StringBuilder methodBuilder = new StringBuilder();
		methodBuilder.append("private void copyData(");
		methodBuilder.append(destName);
		methodBuilder.append(" ");
		methodBuilder.append(destNameL);
		methodBuilder.append(",");
		methodBuilder.append(sourceName);
		methodBuilder.append(" ");
		methodBuilder.append(sourceNameL);
		methodBuilder.append(") {\n");
		for (Field field:destFields) {
			methodBuilder.append("\t");
			methodBuilder.append(destNameL);
			methodBuilder.append(".set");
			methodBuilder.append(toUpperCaseFirstOne(field.getName()));
			methodBuilder.append("(");
			methodBuilder.append(sourceNameL);
			methodBuilder.append(".get");
			methodBuilder.append(toUpperCaseFirstOne(field.getName()));
			methodBuilder.append("()");
			methodBuilder.append(");\n");
		}
		methodBuilder.append("}");
		System.out.println(methodBuilder.toString());
	}
	
	
	
	//首字母转小写
	public static String toLowerCaseFirstOne(String s){
	  if(Character.isLowerCase(s.charAt(0)))
	    return s;
	  else
	    return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
	}


	//首字母转大写
	public static String toUpperCaseFirstOne(String s){
	  if(Character.isUpperCase(s.charAt(0)))
	    return s;
	  else
	    return (new StringBuilder()).append(Character.toUpperCase(s.charAt(0))).append(s.substring(1)).toString();
	}
	
	public static class UserVo{
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Integer getAge() {
			return age;
		}
		public void setAge(Integer age) {
			this.age = age;
		}
		String name;
		Integer age;
	}
	
	public static class UserDo{
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Integer getAge() {
			return age;
		}
		public void setAge(Integer age) {
			this.age = age;
		}
		public Date getCretaedDate() {
			return cretaedDate;
		}
		public void setCretaedDate(Date cretaedDate) {
			this.cretaedDate = cretaedDate;
		}
		String name;
		Integer age;
		Date cretaedDate;
	}
}


